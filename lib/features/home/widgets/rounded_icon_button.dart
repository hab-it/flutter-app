import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RoundedIconButton extends StatelessWidget {
  final IconData icon;
  final Function onPress;
  final double iconSize;
  final Color iconColor;
  final Color backgroundColor;

  RoundedIconButton(
      {@required this.icon,
      @required this.onPress,
      @required this.iconSize,
      @required this.iconColor,
      @required this.backgroundColor});

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      constraints: BoxConstraints.tightFor(width: iconSize, height: iconSize),
      elevation: 6.0,
      onPressed: onPress,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(iconSize * 0.2)),
      fillColor: backgroundColor,
      child: Icon(
        icon,
        color: iconColor,
        size: iconSize * 0.8,
      ),
    );
  }
}
