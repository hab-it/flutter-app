import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hab_it/features/home/widgets/custom_stepper.dart';
import 'package:hab_it/features/home/widgets/rounded_icon_button.dart';

class HabitItemNumber extends StatefulWidget {
  final String title;
  final String description;
  final int value;

  const HabitItemNumber(
      {@required this.title, @required this.description, @required this.value});

  @override
  _HabitItemNumberState createState() => _HabitItemNumberState();
}

class _HabitItemNumberState extends State<HabitItemNumber> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15.0),
      width: 350,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Color(0xfff5f5f5),
      ),
      child: Row(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(widget.title,
                  style: GoogleFonts.nunito(
                      textStyle: TextStyle(
                          fontWeight: FontWeight.w800, fontSize: 18.0))),
              Text(widget.description,
                  style: GoogleFonts.nunito(
                      textStyle: TextStyle(
                          fontWeight: FontWeight.w400, fontSize: 18.0))),
            ],
          ),
          Spacer(),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CustomStepper(minLimit: 0, maxLimit: 6, value: 2)
            ],
          )
        ],
      ),
    );
  }
}
