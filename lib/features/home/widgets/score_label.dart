import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ScoreLabel extends StatefulWidget {
  final int score;

  const ScoreLabel({@required this.score});

  @override
  _ScoreLabelState createState() => _ScoreLabelState();
}

class _ScoreLabelState extends State<ScoreLabel> {
  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
        ),
        child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            child: RichText(
              text: new TextSpan(
                // Note: Styles for TextSpans must be explicitly defined.
                // Child text spans will inherit styles from parent
                style: new TextStyle(
                  fontSize: 14.0,
                  color: Colors.black,
                ),
                children: <TextSpan>[
                  new TextSpan(
                      text: widget.score.toString(),
                      style: GoogleFonts.nunito(
                          textStyle: TextStyle(fontWeight: FontWeight.w800))),
                  new TextSpan(
                      text: ' points',
                      style: GoogleFonts.nunito(
                          textStyle: TextStyle(fontWeight: FontWeight.w500))),
                ],
              ),
            )));
  }
}
