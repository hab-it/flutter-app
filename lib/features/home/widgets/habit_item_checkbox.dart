import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class HabitItemCheckbox extends StatefulWidget {
  final String title;
  final String description;
  bool value;

  HabitItemCheckbox(
      {@required this.title, @required this.description, @required this.value});

  @override
  _HabitItemCheckboxState createState() => _HabitItemCheckboxState();
}

class _HabitItemCheckboxState extends State<HabitItemCheckbox> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15.0),
      width: 350,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Color(0xfff5f5f5),
      ),
      child: Row(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(widget.title,
                  style: GoogleFonts.nunito(
                      textStyle: TextStyle(
                          fontWeight: FontWeight.w800, fontSize: 18.0))),
              Text(widget.description,
                  style: GoogleFonts.nunito(
                      textStyle: TextStyle(
                          fontWeight: FontWeight.w400, fontSize: 18.0))),
            ],
          ),
          Spacer(),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Checkbox(
                  activeColor: Colors.black,
                  value: widget.value,
                  onChanged: (bool value) {
                    setState(() {
                      widget.value = !widget.value;
                    });
                  })
            ],
          )
        ],
      ),
    );
  }
}
