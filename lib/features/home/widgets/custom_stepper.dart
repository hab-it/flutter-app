import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hab_it/features/home/widgets/rounded_icon_button.dart';

class CustomStepper extends StatefulWidget {
  final int minLimit;
  final int maxLimit;
  int value;

  CustomStepper(
      {@required this.minLimit, @required this.maxLimit, @required this.value});

  @override
  _CustomStepperState createState() => _CustomStepperState();
}

class _CustomStepperState extends State<CustomStepper> {
  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        RoundedIconButton(
          icon: Icons.remove_rounded,
          onPress: () {
            setState(() {
              widget.value = widget.value == widget.minLimit
                  ? widget.minLimit
                  : widget.value -= 1;
            });
          },
          iconSize: 24.0,
          iconColor: Colors.black,
          backgroundColor: Colors.white,
        ),
        Text('${widget.value}',
            style: GoogleFonts.nunito(
                textStyle:
                    TextStyle(fontWeight: FontWeight.w800, fontSize: 18.0))),
        RoundedIconButton(
          icon: Icons.add_rounded,
          onPress: () {
            setState(() {
              widget.value = widget.value == widget.maxLimit
                  ? widget.maxLimit
                  : widget.value += 1;
            });
          },
          iconSize: 24.0,
          iconColor: Colors.white,
          backgroundColor: Colors.black,
        ),
      ],
    );
  }
}
