import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hab_it/features/home/widgets/habit_item_checkbox.dart';
import 'package:hab_it/features/home/widgets/habit_item_number.dart';
import 'package:hab_it/features/home/widgets/score_label.dart';
import 'package:date_picker_timeline/date_picker_timeline.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  DateTime _selectedDate = DateTime.now();

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: new BoxDecoration(
            gradient: new LinearGradient(colors: [
          const Color(0xFFF4D2B6),
          const Color(0xFFF2B8A4),
        ])),
        child: Column(
          children: <Widget>[
            Flexible(
              flex: 1,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                      padding:
                          const EdgeInsets.only(top: 55, bottom: 20, left: 20),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          textDirection: TextDirection.ltr,
                          children: <Widget>[
                            Text('Hey',
                                style: GoogleFonts.nunito(
                                    textStyle: TextStyle(
                                        fontWeight: FontWeight.w700,
                                        fontSize: 24.0))),
                            Text('John Doe',
                                style: GoogleFonts.nunito(
                                    textStyle: TextStyle(
                                        fontWeight: FontWeight.w700,
                                        fontSize: 36.0))),
                          ])),
                  Expanded(flex: 1, child: Container()),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 55, bottom: 20, right: 20),
                    child: ScoreLabel(score: 350),
                  )
                ],
              ),
            ),
            Flexible(
              flex: 4,
              child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: const Radius.circular(30.0),
                        topRight: const Radius.circular(30.0)),
                    color: Colors.white,
                  ),
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(bottom: 30),
                          child: DatePicker(
                            DateTime.now(),
                            initialSelectedDate: DateTime.now(),
                            selectionColor: Colors.black,
                            selectedTextColor: Colors.white,
                            onDateChange: (date) {
                              // New date selected
                              setState(() {
                                _selectedDate = date;
                              });
                            },
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 15),
                          child: Text('Habits',
                              style: GoogleFonts.nunito(
                                  textStyle: TextStyle(
                                      fontWeight: FontWeight.w700,
                                      fontSize: 24.0))),
                        ),
                        Padding(
                            padding: const EdgeInsets.only(bottom: 15),
                            child: HabitItemCheckbox(
                                title: 'Go for a walk',
                                description: 'Take in the fresh air.',
                                value: true)),
                        Padding(
                            padding: const EdgeInsets.only(bottom: 15),
                            child: HabitItemNumber(
                                title: 'Drink water',
                                description: 'Bottle up!',
                                value: 3)),
                        Padding(
                            padding: const EdgeInsets.only(bottom: 15),
                            child: HabitItemCheckbox(
                                title: 'Compliment someone',
                                description: 'Everybody loves to hear them.',
                                value: false)),
                      ])),
            )
          ],
        ));
  }
}
