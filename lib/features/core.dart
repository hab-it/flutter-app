import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_snake_navigationbar/flutter_snake_navigationbar.dart';
import 'package:hab_it/features/community/pages/community_page.dart';
import 'package:hab_it/features/settings/pages/settings_page.dart';

import 'challenges/pages/challenges_page.dart';
import 'feed/pages/feed_page.dart';
import 'home/pages/home_page.dart';

class Core extends StatefulWidget {
  @override
  _CoreState createState() => _CoreState();
}

class _CoreState extends State<Core> {
  ShapeBorder bottomBarShape = const RoundedRectangleBorder(
    borderRadius: BorderRadius.all(Radius.circular(25)),
  );
  SnakeBarBehaviour snakeBarStyle = SnakeBarBehaviour.pinned;
  EdgeInsets padding = const EdgeInsets.all(0);

  int _selectedItemPosition = 2;
  SnakeShape snakeShape = SnakeShape.circle;

  bool showSelectedLabels = false;
  bool showUnselectedLabels = false;

  Color selectedColor = Colors.black;
  Color unselectedColor = Color(0xffd0d0d5);

  PageController pageController = PageController(
    initialPage: 2,
    keepPage: true,
  );

  @override
  Widget build(BuildContext context) {
    // final pages = [HomePage()];
    return Scaffold(
        body: AnimatedContainer(
          color: Colors.white,
          duration: const Duration(seconds: 1),
          child: PageView(
            controller: pageController,
            onPageChanged: (index) {
              pageChanged(index);
            },
            children: <Widget>[
              FeedPage(),
              ChallengesPage(),
              HomePage(),
              CommunityPage(),
              SettingPage()
            ],
          ),
        ),
        bottomNavigationBar: SnakeNavigationBar.color(
          behaviour: snakeBarStyle,
          snakeShape: snakeShape,
          shape: bottomBarShape,
          padding: padding,
          snakeViewColor: selectedColor,
          selectedItemColor:
              snakeShape == SnakeShape.indicator ? selectedColor : null,
          unselectedItemColor: unselectedColor,
          showUnselectedLabels: showUnselectedLabels,
          showSelectedLabels: showSelectedLabels,
          currentIndex: _selectedItemPosition,
          onTap: (index) {
            bottomTapped(index);
          },
          items: [
            BottomNavigationBarItem(icon: Icon(Icons.explore), label: 'feed'),
            BottomNavigationBarItem(
                icon: Icon(Icons.calendar_today_rounded), label: 'challenges'),
            BottomNavigationBarItem(
                icon: Icon(Icons.home_rounded), label: 'home'),
            BottomNavigationBarItem(
                icon: Icon(Icons.people_alt_rounded), label: 'community'),
            BottomNavigationBarItem(
                icon: Icon(Icons.settings), label: 'settings')
          ],
        ));
  }

  void pageChanged(int index) {
    setState(() {
      _selectedItemPosition = index;
    });
  }

  void bottomTapped(int index) {
    pageController.animateToPage(index,
        duration: Duration(milliseconds: 500), curve: Curves.ease);
  }
}

class PagerPageWidget extends StatelessWidget {
  final String text;
  final String description;
  final Image image;
  final TextStyle titleStyle =
      const TextStyle(fontSize: 40, fontFamily: 'SourceSerifPro');
  final TextStyle subtitleStyle = const TextStyle(
    fontSize: 20,
    fontFamily: 'Nunito',
    fontWeight: FontWeight.w200,
  );

  const PagerPageWidget({
    Key key,
    this.text,
    this.description,
    this.image,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(24),
      child: SafeArea(
        child: OrientationBuilder(builder: (context, orientation) {
          return orientation == Orientation.portrait
              ? _portraitWidget()
              : _horizontalWidget(context);
        }),
      ),
    );
  }

  Widget _portraitWidget() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text(text, style: titleStyle),
            const SizedBox(height: 16),
            Text(description, style: subtitleStyle),
          ],
        ),
        image
      ],
    );
  }

  Widget _horizontalWidget(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Container(
          width: MediaQuery.of(context).size.width / 2,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Text(text, style: titleStyle),
              Text(description, style: subtitleStyle),
            ],
          ),
        ),
        Expanded(child: image)
      ],
    );
  }
}
