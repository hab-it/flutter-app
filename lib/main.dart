import 'package:flutter/material.dart';
import 'features/core.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'HabIt',
      theme: ThemeData(
        fontFamily: 'Nunito',
        primaryColor: Color(0xffF38C8F),
        accentColor: Color(0xfff2b8a4),
      ),
      home: Core(),
    );
  }
}
